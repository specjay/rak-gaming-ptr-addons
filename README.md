# Must have addons

Everything here works fine on our Vanilla Emulator.

1. Download this project
2. Extract contents to `Interface/AddOns` folder

### Libraries

Working libraries:

* [Ace2] - the one and only cool lua framework back in 2006
* [Healcomm] - i guess this helps not to overheal

### Addons

Raider's basics:

* [DPSMate] - your new favorite DPS meter and analytics
* [Decursive] - awesome decurse helper, bind-and-spam enabled
* [KLHThreatMeter] - your should know how far to push
* [BigWigs] - nothing to do here without BossMods
